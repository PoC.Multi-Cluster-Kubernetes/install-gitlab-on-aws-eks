# Install GitLab on AWS EKS 


## Prerequisites
- Installing and configuring AWS CLI
- Terraform
- Kubectl
- Helm cli
- Create a public hosted zone in Route 53
- Request a public certificate with AWS Certificate Manager


## Getting started

```
export AWS_ACCESS_KEY_ID=...
export AWS_SECRET_ACCESS_KEY=...
export AWS_DEFAULT_REGION=us-east-1

eksctl create cluster \
  --version 1.23 \
  --region us-east-1 \
  --node-type t3.medium \
  --nodes 5 \
  --nodes-min 1 \
  --nodes-max 5 \
  --name my-first-cluster

aws eks update-kubeconfig --region us-east-1 --name my-first-cluster
```


## Sites

- [ ] [Deploy GitLab with Terraform](https://dev.to/stack-labs/deploying-production-ready-gitlab-on-amazon-eks-with-terraform-3coh)
https://gitlab.com/Chabane87/article-gitlab-eks
